<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/bulona/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Aug 2019 04:42:57 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Blood_Bank : index</title>
  <!--favicon-->
  <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
  <!-- Vector CSS -->
  <link href="plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
  <!-- simplebar CSS-->
  <link href="plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="css/app-style.css" rel="stylesheet"/>
  
</head>

<body>

   <!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>
   <!-- end loader -->
<?php 
include 'include/sidebar.php';
?>

<?php 
include 'include/header.php';
?>
<!--Start Dashboard Content-->
<div class="clearfix"></div>
  
  <div class="content-wrapper">
    <div class="container-fluid">

     




	<?php 
include 'include/footer.php';
?>
	
   <!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
      </ul>
      
     </div>
   </div>
  <!--end color cwitcher-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
	
 <!-- simplebar js -->
  <script src="plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="js/sidebar-menu.js"></script>
  <!-- loader scripts -->
  <script src="js/jquery.loading-indicator.html"></script>
  <!-- Custom scripts -->
  <script src="js/app-script.js"></script>
  <!-- Chart js -->
  
  <script src="plugins/Chart.js/Chart.min.js"></script>
  <!-- Vector map JavaScript -->
  <script src="plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- Easy Pie Chart JS -->
  <script src="plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
  <!-- Sparkline JS -->
  <script src="plugins/sparkline-charts/jquery.sparkline.min.js"></script>
  <script src="plugins/jquery-knob/excanvas.js"></script>
  <script src="plugins/jquery-knob/jquery.knob.js"></script>
    
    <script>
        $(function() {
            $(".knob").knob();
        });
    </script>
  <!-- Index js -->
  <script src="js/index.js"></script>

  
</body>

<!-- Mirrored from codervent.com/bulona/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Aug 2019 04:44:10 GMT -->
</html>
