<!DOCTYPE html>
<html>
<head>
	    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
	body
            {
              margin: 0;
              padding: 0;
              overflow-x: hidden;
            }
        .sss{
      color:red;
    }
</style>
</head>
<title>Members</title>
<body>
	 <?php 
include 'index_fetch.php';
?>

	
       <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-Members" role="tabpanel" aria-labelledby="nav-Members-tab">
          <div class="bd-example">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-interval="1500">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner ">
          <div class="carousel-item active ">
            <img src="..\images\hulk.jpg" class="d-block w-100" alt="1">
            <div class="carousel-caption d-none d-md-block sss"><br><br><br>
              <h4>John Hagins</h4>
            <h5>President & Chief Executive Officer</h5>
            
            </div>
          </div>
           <div class="carousel-item">
            <img src="..\images\thor.jpg" class="d-block w-100" alt="2">
            <div class="carousel-caption d-none d-md-block sss">
              <h4>Michael Parejko</h4>
               <h5>Senior Manager at Blood Center</h5>
               <h6>Michael Parejko was appointed CEO March 27, 2017.He brings more than 20 years of experience helping businesses transform and grow.</h6>
            </div>
          </div>
          <div class="carousel-item">
            <img src="..\images\america.jpg" class="d-block w-100" alt="3">
            <div class="carousel-caption d-none d-md-block sss">
              <h4>Jeannie Mascolino</h4>
              <h5>Director of Operations</h5>
              <h6>Jeannie Mascolino gained invaluable insights into the company’s business and markets. </h6>
            </div>
          </div>
          <div class="carousel-item">
            <img src="..\images\iron.png" class="d-block w-100" alt="4">
            <div class="carousel-caption d-none d-md-block sss">
              <h4>Rich Miller-Murphy</h4>
              <h5>Director at Blood Center</h5>
              <h6>Rich Miller-Murphy earned a Bachelor of Science in Economics from The Wharton School at the University of Pennsylvania and an MBA from Harvard Business School.</h6>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div></div>


</body>
</html>