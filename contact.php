<!doctype html>
  <html lang="en">
  <head>
     <link rel="shortcut icon" href="img/favicon.png" type="image/png">
        <link rel="icon" href="images/fv.png" sizes="5*5" type="image/png">
  	<title>Blood Bank  : Contact</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<style>
   body{
           background:linear-gradient(0deg,rgba(0,0,100,0.4),rgba(0,0,100,0.2)),url(images/j.jpg);
          background-size:cover;
            }
	
	.z{
        background-color:navy;
            height: 71px;
      }

    .s{

            font:bold 19px Arial Rounded MT;
      }

  .bs{
              width: 50px;
              margin-left: -17px;
              margin-bottom: -6px;

     }
     
 .col1{

height: 50px;

margin-top: -30px;
padding-top: -1px;
margin-left: -60px;
 }
  .col2{

height: 50px;
margin-top: -30px;
margin-left: -60px;

 }
 .heading{
 	color: black;
 	font-style: Arial Rounded MT;
 	font-size: 28px;
 	text-align: center;
 font-weight:900; 
 margin-right: 150px;
 margin-left: -20px;margin-top: 20px;
 }
 .heading2{
 	color: black;
 	font-style: Arial Rounded MT;
 	font-size: 28px;
 	text-align: center;
 font-weight:900; 
 margin-top: 20px;
 margin-left: -150px;

 }
 .margin{
  padding-top: 20px;

 }
 .text{
  font-size: 16px;
  font-weight: 800;
 }
 .dd{
  font-style: Arial Rounded MT;
  font-size: 16px;
  font-weight:800; 
 }

    .fa {
        padding: 10px;
        font-size: 20px;
        width: 40px;
        text-align: center;
        text-decoration: none;
        margin: 7px 170px;
      }
.first{
  margin-left: -170px;
  margin-top: -5px;
}

      .fa:hover {
          opacity: 5  ;
      }

      .fa-facebook {
        background: #3B5998;
        color: white;
      }

      .fa-twitter {
        background: #55ACEE;
        color: white;
      }

      .fa-google {
        background: #dd4b39;
        color: white;
      }

      .fa-linkedin {
        background: #007bb5;
        color: white;
      }

      .fa-youtube {
        background: #bb0000;
        color: white;
      }

      .fa-instagram {
        background: #125688;
        color: white;
      }
      .col3{
        margin-left: 500px;
      }
      .heading3{
          color: black;
  font-style: Arial Rounded MT;
  font-size: 28px;
  text-align: center;
   font-weight:900; 
margin-top: -35px;
margin-left:140px;

      }
      .maps{
        margin-top: 5px;
      }
 

</style>
<body>
    <?php include('include/header.php')?>
        <br>
        <br>
        <br>
        <br>

       <div class="container container">
       	<div class="row wall"><br>
       		<div class="col-lg-4 col1">
       			<div class="heading">Contact us</div><br><br>


            <div class="margin">
       	<i class="material-icons" style="font-size:35px">email</i>
         <div class="text">bloodbank14.@gmail.com</div>
          </div>



            <div class="margin">
       	     <i class="material-icons" style="font-size:35px">call</i>
           <div class="text">+916287583621 </div>
         </div>

            <div class="margin">
           <i class="material-icons" style="font-size:35px">message</i>
           <div class="text">+918435925392</div>
             </div>

            <div class="margin">
             <i class="material-icons" style="font-size:35px">business</i>
               <div class="text">Main Road, Akaltara, Janjgir-Champa, 495661, Near Railway Station</div>
          </div>

            
                   


       		</div>
       		<div class="col-lg-4 col2">
       			<div class="heading2">Connect us</div><br><br><br>


              <div class="space">
              <div class="first"><a href="https://Facebook.com" class="fa fa-facebook"></a></div><span class="dd"> Like us on Facebook</span> <br>
                <div class="first"> <a href="https://twitter.com/login?lang=en" class="fa fa-twitter"></a>  </div> <span class="dd">Follow us on Twitter</span><br>
                 <div class="first"><a href="https://aboutme.google.com/u/0/?referer=gplus" class="fa fa-google"></a>  </div> <span class="dd">Add us on Google Plus</span><br>
                 <div class="first"><a href="https://in.linkedin.com" class="fa fa-linkedin"></a>  </div><span class="dd"> Follow us on Linkedin</span><br> 
                 <div class="first"><a href="https://www.youtube.com/" class="fa fa-youtube"></a>   </div><span class="dd">Add us on Youtube</span><br>
                 <div class="first"><a href="https://www.instagram.com/" class="fa fa-instagram"></a>  </div><span class="dd"> Follow us on Instagram</span>
         </div>
       		</div>
        </div>


    
<div class="col-lg-4 col3">
    <div class="heading3">locate on Map</div>
 
<br><br><br><div class="maps">
  <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14877.923765297675!2d81.84977085000001!3d21.21276905!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1557408339643!5m2!1sen!2sin" width="700" height="400" frameborder="0" style="border:0" allowfullscreen></iframe></div<br><br><br><br><br><br><br><br><br><br>

 



</div>
</div>

        
</body>
</html>