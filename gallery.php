
  <!doctype html>
  <html lang="en">
  <head>
     <link rel="shortcut icon" href="img/favicon.png" type="image/png">
        <link rel="icon" href="images/fv.png" sizes="5*5" type="image/png">
  	<title>Blood Bank : Gallery</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
  
    	

    	
    	 .z{
            background-color:navy;
            height: 71px;
            
          }
          .s{

            font:bold 19px Arial Rounded MT;
          }
 .bs{
              width: 50px;
              margin-left: -17px;
              margin-bottom: -6px;

               }


    	body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
 padding: 0 8px; 
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}


.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 60%;
  height: 100%;
  overflow: auto;
  background-color: black;
}


.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}


.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}


.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}


.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}


.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}


.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 10;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.Heading{
	font-size: 22px;
	font-family: Arial Rounded MT;
	text-align: center;
  background-color: orange;
  height: 40px;
  margin-top: 10px; 
padding-top: 3px;
margin-bottom: -12px;
}
.a{
	margin-bottom: -25px;

}
</style>
	  

    
</head>
<body>
 <?php include('include/header.php')?><br><br><br>
<div class="Heading">Some Unforgettable Moment</div><br>

<div class="row">
  <div class="column a">
    <img src="images/g1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
   <div class="column">
    <img src="images/g5.png" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g6.jpg" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g7.jpg" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g8.jpg" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g9.jpg" style="width:100%" onclick="openModal();currentSlide(9)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="images/g10.jpg" style="width:100%" onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
  </div>
   
</div>

<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 14</div>
      <img src="images/g1.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 14</div>
      <img src="images/g2.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 14</div>
      <img src="images/g3.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 14</div>
      <img src="images/g4.jpg" style="width:100%">
    </div>
    
       <div class="mySlides">
      <div class="numbertext">5 / 14</div>
      <img src="images/g5.png" style="width:100%">
    </div>

       <div class="mySlides">
      <div class="numbertext">6 / 14</div>
      <img src="images/g6.jpg" style="width:100%">
    </div>

       <div class="mySlides">
      <div class="numbertext">7 / 14</div>
      <img src="images/g7.jpg" style="width:100%">
    </div>  

     <div class="mySlides">
      <div class="numbertext">8 / 14</div>
      <img src="images/g8.jpg" style="width:100%">
    </div> 

      <div class="mySlides">
      <div class="numbertext">9 / 14</div>
      <img src="images/g9.jpg" style="width:100%">
    </div> 

      <div class="mySlides">
      <div class="numbertext">10 / 14</div>
      <img src="images/g10.jpg" style="width:100%">
    </div>  

    

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="column">
      <img class="demo cursor" src="images/g1.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g2.jpg" style="width:100%" onclick="currentSlide(2)" alt="Snow">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g3.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g4.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
    </div>


    <div class="column">
          <img class="demo cursor" src="images/g5.png" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g6.jpg" style="width:100%" onclick="currentSlide(6)" alt="Snow">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g7.jpg" style="width:100%" onclick="currentSlide(7)" alt="Mountains and fjords">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g8.jpg" style="width:100%" onclick="currentSlide(8)" alt="Northern Lights">
    </div>

    <div class="column">
          <img class="demo cursor" src="images/g9.jpg" style="width:100%" onclick="currentSlide(9)" alt="Nature and sunrise">
    </div>

    <div class="column">
      <img class="demo cursor" src="images/g10.jpg" style="width:100%" onclick="currentSlide(10)" alt="Snow">
    </div>

   

  </div>
</div>

    <script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

	</body>
	</html>