
 <!doctype html>
      <html lang="en">
      <head>
        <title>Blood Bank  : About</title>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="shortcut icon" href="img/favicon.png" type="image/png">
        <link rel="icon" href="images/fv.png" sizes="5*5" type="image/png">


       <style>
          body{
           background:linear-gradient(0deg,rgba(0,0,100,0.3),rgba(0,0,100,0.2));
          background-size:cover;
            }

       	.z{
            background-color:navy;
            height: 71px;
            
          }
          .s{

            font:bold 19px Arial Rounded MT;
          }
 .bs{
              width: 50px;
              margin-left: -17px;
              margin-bottom: -6px;

               }
               .image{
               	margin-left:70px;
              
               }

               .col1{
               	background-color: orange;
               	height: 50px;
               	margin-top: -100px;
               	  font: bold 25px Arial Rounded MT;
               	  color:white;
               	  padding-top: 7px;
               	  text-align: center;


               }
               .col2{
               	 	background-color: orange;
               	height: 50px;
               	margin-top: -5px;
               	  font: bold 25px Arial Rounded MT;
               	  color:white;
               	  padding-top: 7px;
               	  text-align: center;



               }
                 .col3{
               	 	background-color: orange;
               	height: 50px;
               	margin-top: -5px;
               	  font: bold 25px Arial Rounded MT;
               	  color:white;
               	  padding-top: 7px;
               	  text-align: center;



               }
                .zz{

          background:linear-gradient(0deg,rgba(0,0,100,0.5),rgba(0,0,100,0)),url(images/f1.jpg);
              
              height: 500px;
}
  .link{
              font:bold 18px Century Gothic;
              color: white;
            }
             .link1{
      color:white;
       font: bold 22px Arial;

    }
    .kk{
  margin-left: 15px;
}
table {
  margin-left:15px;
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 80%

}

td, th {
  border: 2px solid #dddddd;
  text-align: center;
  padding: 5px;
  
}

tr:nth-child(even) {
  color: black;
  background-color:white;
}
 .dd{
         color: white;
         font:bold 18px Arial Rounded MT;}
          .rr{
        margin-top: 2px;
      }
     .man{
            height: 50px;
            color: white;
              padding-top: 17px;
              padding-left: 20px; 
            font:15px Arial Rounded MT;
            background:linear-gradient(0deg,rgba(0,0,100,1),rgba(0,0,100,0.1)),url(images/f1.jpg);
          background-size:cover;  
          text-align: center;
          }
             .fa {
        padding: 10px;
        font-size: 20px;
        width: 40px;
        text-align: center;
        text-decoration: none;
        margin: 5px 2px;
      }
          .fa:hover {
          opacity: 5  ;
      }

      .fa-facebook {
        background: #3B5998;
        color: white;
      }

      .fa-twitter {
        background: #55ACEE;
        color: white;
      }

      .fa-google {
        background: #dd4b39;
        color: white;
      }

      .fa-linkedin {
        background: #007bb5;
        color: white;
      }

      .fa-youtube {
        background: #bb0000;
        color: white;
      }

      .fa-instagram {
        background: #125688;
        color: white;
      }
       </style>
   </head>
        <?php include('include/header.php')?><br><br><br><br>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-md-6 "><br><br><br><br>
        			<div class="col1">Pillers of Blood Bank</div><br><br>


   <div class="image"><img src="images/hulk.jpg" width="500"></div> 
    <h4 style="margin-left: 250px; font-weight: 500px; margin-top: 5px;">John Hagins</h4>

            <h5 style="margin-left: 160px;">President & Chief Executive Officer</h5>
            <h6 style="margin-left: 15px;">John Hagins was appointed CEO May 16, 2016. Previously, he served as a Senior Partner of McKinsey & Company where he led the Global Medical Products Practice.The declining of syphilis rates in developed countries raised the question of whether syphilis screening was still necessary for blood donors. However, in developing countries, the prevalence of positive serologic tests for syphilis (STS) can be as high as 13.5 percent,8 as in Ghana. In such settings, the potential for laboratory or human error missing infections highlights the continued need for universal blood donor screening for syphilis and ongoing assessment of blood donor risk for infection to help ensure a safer blood supply.9 While many questions about syphilis and blood donation are discussed, little systematic information is available on the profile of blood donors with a positive STS, including differences between donors with recent versus past infection. For example, one question that remains is whether blood from donors with histories of past syphilis with low serologic titers should be transfused. While excluding these donations is wasteful, past history of syphilis may be amarker of continuing and undisclosed high-risk sexual behavior, which could lead to HIV infection. </h6>
          <br>
          <div class="image"><img src="images/thor.jpg" width="500"></div> 
    <h4 style="margin-left: 250px; font-weight: 500px; margin-top: 5px;">Michael Parejko</h4>

            <h5 style="margin-left: 180px;">Senior Manager at Blood Center</h5>
            <h6 style="margin-left: 15px">Michael Parejko was appointed CEO March 27, 2017.He brings more than 20 years of experience helping businesses transform and grow.. However, in developing countries, the prevalence of positive serologic tests for syphilis (STS) can be as high as 13.5 percent,8 as in Ghana. In such settings, the potential for laboratory or human error missing infections highlights the continued need for universal blood donor screening for syphilis and ongoing assessment of blood donor risk for infection to help ensure a safer blood supply.9 While many questions about syphilis and blood donation are discussed, little systematic information is available on the profile of blood donors with a positive STS, including differences between donors with recent versus past infection. For example, one question that remains is whether blood from donors with histories of past syphilis with low serologic titers should be transfused. While excluding these donations is wasteful, past history of syphilis may be amarker of continuing and undisclosed high-risk sexual behavior, which could lead to HIV infection. </h6>
        <br>
            <div class="image"><img src="images/america.jpg" width="500"></div> 
    <h4 style="margin-left: 240px; font-weight: 500px; margin-top: 5px;">Jeannie Mascolino</h4>

            <h5 style="margin-left:237px;">Director of Operations</h5>
            <h6 style="margin-left: 15px;">Jeannie Mascolino gained invaluable insights into the company’s business and markets. help ensure a safer blood supply.9 While many questions about syphilis and blood donation are discussed, little systematic information is available on the profile of blood donors with a positive STS, including differences between donors with recent versus past infection. For example, one question that remains is whether blood from donors with histories of past syphilis with low serologic titers should be transfused. While excluding these donations is wasteful, past history of syphilis may be amarker of continuing and undisclosed high-risk sexual behavior, which could lead to HIV infection. </h6><br>
          
            <div class="image"><img src="images/iron.png" width="500"></div> 
    <h4 style="margin-left: 230px; font-weight: 500px; margin-top: 5px;">Rich Miller-Murphy</h4>

            <h5 style="margin-left: 225px;">Director at Blood Center</h5>
            <h6 style="margin-left: 15px">Rich Miller-Murphy earned a Bachelor of Science in Economics from The Wharton School at the University of Pennsylvania and an MBA from Harvard Business School. still necessary for blood donors. However, in developing countries, the prevalence of positive serologic tests for syphilis (STS) can be as high as 13.5 percent,8 as in Ghana. In such settings, the potential for laboratory or human error missing infections highlights the continued need for universal blood donor screening for syphilis and ongoing assessment of blood donor risk for infection to help ensure a safer blood supply. </h6>
          </div>

  

<div class="col-md-6">
	<div class="col2">Our Process</div><br><br>
      	Saving lives for more than 70 years
Blood Bank of Hawaii opened its doors in February 1941 on the grounds of what is now The Queen's Medical Center. Known then as Honolulu Blood-Plasma Bank, the blood bank served as a wartime agency under the Office of Civilian Defense, returning to its non-profit civilian status in 1943. Honolulu Blood-Plasma Bank officially changed its name to Blood Bank of Hawaii in 1946.

In the early years, blood was collected in glass bottles, hemoglobin checks were done via the earlobe, and a single van went around the island to collect blood. Services were expanded to included neighbor island blood drives and Hawaii's unique ethnic population became nationally recognized as a source for many types of rare blood.

Today, state-of-the-art blood bags are used for collections, adequate iron levels are verified through technology, and blood is collected at two fixed sites, along with two bloodmobiles that travel to Oahu's neighborhoods providing convenience for donors. Blood Bank of Hawaii adheres to the highest standards of safety and quality and continues its long commitment to providing a safe and adequate blood supply for Hawaii's patients.

Blood Bank of Hawaii is accredited by AABB and is a member of America’s Blood Centers (ABC), the nation’s largest network of community-based blood programs. Blood Bank of Hawaii is licensed and regulated by U.S. Food and Drug Administration.<br><br>
     <div class="col3">Our Mission</div><br>
     This website presents a high-end system to bridge the gap between the blood donors and the people in need for blood. Website for Blood Bank is a way to synchronize Blood banks  with the help of Internet. It is a Web Application through which Registered user can check the availability of required Blood and can send Request for blood to the nearest blood bank or donee can be ordered online as and when required. The location of the blood bank can also be traced using maps.This website make a bridge between the blood donors and the people in need for blood. It is a way to synchronize Blood banks with the help of Internet. The main purpose of this system to saving the time and distance.
     	
    </div>

  </div>

</div>
<!-- <?php include('include/footer.php')?>  -->

          <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
         </body>

        </html>