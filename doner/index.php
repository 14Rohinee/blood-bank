<!DOCTYPE html>
<html lang="en">
<head>
	<title>Criteria to donate blood</title>
    <meta name="keywords" content="">
	<meta name="description" content="">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 
OneTel Template
http://www.templatemo.com/tm-468-onetel
-->
	<!-- stylesheet css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/nivo-lightbox.css">
	<link rel="stylesheet" href="css/nivo_themes/default/default.css">
	<link rel="stylesheet" href="css/templatemo-style.css">
	<!-- google web font css -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<style>
		.button {
/*  background-color:orange;
  border: none;
  color: black;
 padding-top: 10px;
 padding-bottom: 10px;
 padding-left: 10px;
 padding-right: 10px;
  text-align: center;
  font-size: 16px;
  cursor: pointer;*/
  margin-left: 600px;
  margin-bottom: 10px;

}

/*.button:hover {
  background-color: darkorange;
}*/
	</style>

</head>
<body>
	

<!-- home section --><br>

	
			
				<center><h1>Criteria to donate blood</h1></center><br><br><br><br>
	
<!-- divider section -->
<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="divider-wrapper divider-one">
					<i class="fa fa-">1</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Overall health</h2></a>
					
					<p>The donor must be fit and healthy, and should not be suffering from transmittable diseases.</p>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-two">
					<i class="fa fa-">2</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Age and weight</h2></a>
					<p>The donor must be 18–65 years old and should weigh a minimum of 50 kg.</p>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-three">
					<i class="fa fa">3</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Pulse rate</h2></a>
					<p>The doner must be pulse rate Between 50 and 100 without irregularities.</p>
				</div>
			</div>
		</div>
	</div>
</div><br>
<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="divider-wrapper divider-one">
					<i class="fa fa-">4</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Hemoglobin level</h2></a>
					<p> The doner must be Hemoglobin level minimum of 12.5 g/dL.</p>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-two">
					<i class="fa fa">5</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Blood pressure</h2></a>
					<p>The doner must be Blood pressure in Diastolic: 50–100 mm Hg and Systolic: 100–180 mm Hg.</p>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-three">
					<i class="fa fa">6</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h2>Body temperature</h2></a>
					<p>Body temperature Should be normal, temperature not exceeding 37.5 °C.</p>
				</div>
			</div>
		</div>
	</div>
</div><br>
<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="divider-wrapper divider-one">
					<i class="fa fa-">7</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>A person who has been tested HIV positive.</h3></a>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-two">
					<i class="fa fa">8</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>The time period between successive blood donations should be more than 3 months.</h3></a>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-three">
					<i class="fa fa">9</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>A person who has consumed alcohol in the past 24 hours.</h3></a>
			
				</div>
			</div>
		</div>
	</div>
</div><br>
<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="divider-wrapper divider-one">
					<i class="fa fa-">10</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>Women who have had miscarriage in the past 6 months.</h3></a>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-two">
					<i class="fa fa">11</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>Individuals who have had fits, tuberculosis, asthma and allergic disorders in the past.</h3></a>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="divider-wrapper divider-three">
					<i class="fa fa">12</i>
					<a href="https://en.wikipedia.org/wiki/Blood_donation_in_India"><h3>Women who are pregnant or breastfeeding.</h3></a>
				</div>
			</div>
		</div>
	</div>
</div>
   
<button class="button"><a href="../form_join.php">i'm eligible </a></button>&nbsp&nbsp&nbsp&nbsp
<button class="button2"><a href="../index.php">i'm not eligible</a></button>





<!-- javascript js -->	
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>	
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>